= ...

----
Fable TLC from steam
Category: Main > Games > Role Playing Games > Fable: The Lost Chapters > 1.0
https://appdb.winehq.org/objectManager.php?sClass=version&iId=3827

A problem occurred initializing DirectDraw. Hardware acceleration may be disabled, please run DXDIAG.

http://steamcommunity.com/app/204030/discussions/0/540737262355434619/
http://forums.anandtech.com/showthread.php?t=2262780\
http://sol.gfxile.net/ddhack/
----

----
git bisect start
git bisect good  wine-1.7.31
git bisect bad  wine-1.7.32
git bisect log

CC="ccache gcc -m32" ./configure --verbose --disable-tests && make -j9
git bisect bad
git bisect bad
----

----
iwana@hadean.abraxas.za.net:/var/opt/dpool000/projects/winehq.org/git/wine-bisect
$ git bisect log
git bisect start
# good: [6d6dd3c42b96bb813d5366b97fbff80441e75918] Release 1.7.31.
git bisect good 6d6dd3c42b96bb813d5366b97fbff80441e75918
# bad: [fe2466ffdfa505329d009dac14cf933e77a14495] Release 1.7.32.
git bisect bad fe2466ffdfa505329d009dac14cf933e77a14495
# bad: [bcce460f9ed667e037afade8882682c310c516c5] mshtml: Added missing nsIHttpChannelInternal functions.
git bisect bad bcce460f9ed667e037afade8882682c310c516c5
# bad: [e6cd0f7d8c4ad51135e0d56161258beb90905fca] dwrite/tests: Some tests for ReadFileFragment().
git bisect bad e6cd0f7d8c4ad51135e0d56161258beb90905fca
# good: [609d43e4086188838e0900477db5651709ef36be] server: Avoid leaking file descriptors if create_console_input fails.
git bisect good 609d43e4086188838e0900477db5651709ef36be
# bad: [d6bdae11df7a4481a68082e54d75360ecbd82236] user32/tests: Add single-threaded tests for WM_[GET|SET]TEXT and [Get|Set]WindowText.
git bisect bad d6bdae11df7a4481a68082e54d75360ecbd82236
# good: [33976b4f445c1991988e999544909330f5fe9844] d3d8/tests: Test messages on focus loss.
git bisect good 33976b4f445c1991988e999544909330f5fe9844
# bad: [0cf86baf65e265fe81d93c5299a325bf658b15c9] user32/tests: Remove unnecessary char array and add additional test in test_FindWindowEx.
git bisect bad 0cf86baf65e265fe81d93c5299a325bf658b15c9
# bad: [9f36db8fa5a863971542e524c7546ed88def61dc] wined3d: Add NVIDIA GeForce GTX 970 to supported device list.
git bisect bad 9f36db8fa5a863971542e524c7546ed88def61dc
# bad: [f6f4af46db7514261167cfd0db3b605a2a0491df] d3d8/tests: Test style changes on focus loss.
git bisect bad f6f4af46db7514261167cfd0db3b605a2a0491df
# first bad commit: [f6f4af46db7514261167cfd0db3b605a2a0491df] d3d8/tests: Test style changes on focus loss.
----

----
iwana@hadean.abraxas.za.net:/var/opt/dpool000/projects/winehq.org/git/wine-bisect
$ git bisect log
git bisect start
# good: [33976b4f445c1991988e999544909330f5fe9844] d3d8/tests: Test messages on focus loss.
git bisect good 33976b4f445c1991988e999544909330f5fe9844
# bad: [0cf86baf65e265fe81d93c5299a325bf658b15c9] user32/tests: Remove unnecessary char array and add additional test in test_FindWindowEx.
git bisect bad 0cf86baf65e265fe81d93c5299a325bf658b15c9
# bad: [9f36db8fa5a863971542e524c7546ed88def61dc] wined3d: Add NVIDIA GeForce GTX 970 to supported device list.
git bisect bad 9f36db8fa5a863971542e524c7546ed88def61dc
# good: [f6f4af46db7514261167cfd0db3b605a2a0491df] d3d8/tests: Test style changes on focus loss.
git bisect good f6f4af46db7514261167cfd0db3b605a2a0491df
# first bad commit: [9f36db8fa5a863971542e524c7546ed88def61dc] wined3d: Add NVIDIA GeForce GTX 970 to supported device list.
----

== source

----
bad commit:
https://github.com/wine-mirror/wine/commit/9f36db8fa5a863971542e524c7546ed88def61dc
bad line:
https://github.com/wine-mirror/wine/blob/master/dlls/wined3d/directx.c#L1273
----

== build

----
CC="ccache gcc -m32" ./configure --verbose --disable-tests && make -j9
----

== debug

----
http://wiki.winehq.org/DebugChannels
http://wiki.winehq.org/Disassembly
http://wiki.winehq.org/Backtraces
https://www.winehq.org/docs/winedev-guide/dbg-control
https://www.winehq.org/docs/winedev-guide/dbg-commands
https://www.winehq.org/docs/winedev-guide/wine-debugger
http://directory.google.com/Top/Computers/Programming/Disassemblers/DOS_and_Windows/

info share

mknod /tmp/debug_pipe p
cat /tmp/debug_pipe
WINEDEBUG=+relay,+snoop,+loaddll,+message,+msgbox wine-steam-fable wine Fable.exe &>/tmp/debug_pipe
cat /tmp/debug_pipe >/tmp/wine.log

WINEDEBUG=+relay,+snoop,+loaddll,+message,+msgbox wine-steam-fable wine Fable.exe &>/tmp/wine.log
WINEDEBUG=+seh,+tid,+relay,+snoop,+loaddll,+message,+msgbox wine-steam-fable wine Fable.exe &>/tmp/wine.log

WINEDEBUG=+all wine-steam-fable wine Fable.exe &>/tmp/wine-ddraw-20150317-debug-all-bad.log

WINEDEBUG=+seh,+tid,+relay,+snoop,+loaddll,+message,+msgbox wine-steam-fable wine Fable.exe &>/tmp/wine-ddraw-20150317-debug-v100-bad.log
WINEDEBUG=+seh,+tid,+relay,+snoop,+loaddll,+message,+msgbox wine-steam-fable wine Fable.exe &>/tmp/wine-ddraw-20150317-debug-v100-good.log


WINEDEBUG=+relay,+snoop,+loaddll,+message,+msgbox wine-steam-fable wine Fable.exe &>/tmp/wine-ddraw-20150317-debug-v50-bad.log
WINEDEBUG=+relay,+snoop,+loaddll,+message,+msgbox wine-steam-fable wine Fable.exe &>/tmp/wine-ddraw-20150317-debug-v50-good.log

WINEDEBUG=+relay wine-steam-fable wine Fable.exe &>/tmp/wine-ddraw-20150317-debug-relay-bad.log
WINEDEBUG=+relay wine-steam-fable wine Fable.exe &>/tmp/wine-ddraw-20150317-debug-relay-good.log

vimdiff /tmp/wine-ddraw-20150317-debug-v100-bad.log <(head -1506324 /tmp/wine-ddraw-20150317-debug-v100-good.log)
diff /tmp/wine-ddraw-20150317-debug-v100-bad.log <(head -1506324 /tmp/wine-ddraw-20150317-debug-v100-good.log) | view -

/tmp/wine-ddraw-20150317-debug-v100-bad.log                                  1467274,74     97%
/proc/20020/fd/63                                                           1477358,74     98%

/tmp/wine-ddraw-20150317-debug-v100-bad.log                                  1467829,100    97%
/proc/20020/fd/63                                                           1477435,62     98%

vimdiff <(sed -n '1467000,1477435p' /tmp/wine-ddraw-20150317-debug-v100-good.log) <(sed -n '1467000,1467829p' /tmp/wine-ddraw-20150317-debug-v100-bad.log)


wine-steam-fable winedbg Fable.exe
break init_driver_info
cont

wine-steam-fable winedbg --gdb Fable.exe
break init_driver_info
continue

wine-steam-fable wine winedbg Fable.exe
break wined3d_init
cont
step
bt
finish
next

wine-steam-fable wine winedbg
break DirectDrawEnumerateExA

----

----
grep -n -e 'KERNEL32.LoadLibraryA.*ddraw.dll' -e 'KERNEL32.lstrcpyA(.*"A problem occurred initializing DirectDraw. Hardware acceleration may be disabled, please run DXDIAG.".*' -e DirectDrawCreateEx -e DirectDrawEnumerateExA /tmp/wine-ddraw-20150317-debug-v100-bad.log
1464294:0009:Call KERNEL32.LoadLibraryA(017c68fc "ddraw.dll") ret=017b5b9b
1464309:0009:Call KERNEL32.GetProcAddress(7d790000,017c68e8 "DirectDrawCreateEx") ret=017b5bb7
1464311:0009:Call KERNEL32.GetProcAddress(7d790000,017c68d0 "DirectDrawEnumerateExA") ret=017b5bcb
1464313:0009:Call ddraw.DirectDrawEnumerateExA(017b5340,00000000,00000001) ret=017b5bde
1465981:0009:Ret  ddraw.DirectDrawEnumerateExA() retval=00000000 ret=017b5bde
1465982:0009:Call ddraw.DirectDrawCreateEx(00000000,002fdaa4,017c6714,00000000) ret=017b5c14
1467444:0009:Ret  ddraw.DirectDrawCreateEx() retval=00000000 ret=017b5c14
1467711:0009:Call KERNEL32.lstrcpyA(013bca38,002fd444 "A problem occurred initializing DirectDraw. Hardware acceleration may be disabled, please run DXDIAG.") ret=0099de45

grep -n -e 'KERNEL32.LoadLibraryA.*ddraw.dll' -e 'KERNEL32.lstrcpyA(.*"A problem occurred initializing DirectDraw. Hardware acceleration may be disabled, please run DXDIAG.".*' -e DirectDrawCreateEx -e DirectDrawEnumerateExA /tmp/wine-ddraw-20150317-debug-all.log
2315625:1372059.097:0009:Call KERNEL32.LoadLibraryA(01c468fc "ddraw.dll") ret=01c35b9b
2316004:1372059.103:0009:Call KERNEL32.GetProcAddress(7d790000,01c468e8 "DirectDrawCreateEx") ret=01c35bb7
2316006:1372059.103:0009:Call KERNEL32.GetProcAddress(7d790000,01c468d0 "DirectDrawEnumerateExA") ret=01c35bcb
2316008:1372059.103:0009:Call ddraw.DirectDrawEnumerateExA(01c35340,00000000,00000001) ret=01c35bde
2316009:1372059.103:0009:trace:ddraw:DirectDrawEnumerateExA callback 0x1c35340, context (nil), flags 0x1.
2316010:1372059.103:0009:trace:ddraw:DirectDrawEnumerateExA Enumerating ddraw interfaces
2327651:1372059.223:0009:trace:ddraw:DirectDrawEnumerateExA Default interface: DirectDraw HAL
2327693:1372059.223:0009:trace:ddraw:DirectDrawEnumerateExA End of enumeration
2327694:1372059.223:0009:Ret  ddraw.DirectDrawEnumerateExA() retval=00000000 ret=01c35bde
2327695:1372059.223:0009:Call ddraw.DirectDrawCreateEx(00000000,0030daa4,01c46714,00000000) ret=01c35c14
2327696:1372059.223:0009:trace:ddraw:DirectDrawCreateEx driver_guid (null), ddraw 0x30daa4, interface_iid {15e65ec0-3b9c-11d2-b92f-00609797ea5b}, outer (nil).
2337946:1372059.264:0009:Ret  ddraw.DirectDrawCreateEx() retval=00000000 ret=01c35c14
2340093:1372059.285:0009:Call KERNEL32.lstrcpyA(013bca38,0030d444 "A problem occurred initializing DirectDraw. Hardware acceleration may be disabled, please run DXDIAG.") ret=0099de45

sed -n '2315625,2340093p' /tmp/wine-ddraw-20150317-debug-all.log > winedebug-000.log
sed -n '1464294,1467711p' /tmp/wine-ddraw-20150317-debug-v100-bad.log > winedebug-001.log
----

----
break GetPixelFormat
info threads

break wined3d_get_adapter_identifier
info share


10005BA3  |. 0F84 C8010000  JE ConfigDe.10005D71
10005BA9  |. 8B35 44600110  MOV ESI,DWORD PTR DS:[<&KERNEL32.GetProcAddress>]         ;  KERNEL32.GetProcAddress
10005BAF  |. 68 E8680110    PUSH ConfigDe.100168E8                                    ; /ProcNameOrOrdinal = "DirectDrawCreateEx"
10005BB4  |. 55             PUSH EBP                                                  ; |hModule
10005BB5  |. FFD6           CALL ESI                                                  ; \GetProcAddress
10005BB7  |. 3BC3           CMP EAX,EBX
10005BB9  |. 894424 20      MOV DWORD PTR SS:[ESP+20],EAX
10005BBD  |. 0F84 AE010000  JE ConfigDe.10005D71
10005BC3  |. 68 D0680110    PUSH ConfigDe.100168D0                                    ; /ProcNameOrOrdinal = "DirectDrawEnumerateExA"
10005BC8  |. 55             PUSH EBP                                                  ; |hModule
10005BC9  |. FFD6           CALL ESI                                                  ; \GetProcAddress


break DirectDrawCreateEx
----

== nemxt

----
err:seh:raise_exception Exception frame is not in stack limits => unable to dispatch exception.
----

